# Open Climate Compiler - Setup files

This repository contains installation scripts to automatically setup the Open Climate Compiler development environment.

## Usage

Move to the directory where you want to setup the development environment, clone this repository and run the `setup.sh` script.
```
cd $OCC_ROOT
git clone git@spclgitlab.ethz.ch:open-climate-compiler/setup.git
# or alternatively
#https://spclgitlab.ethz.ch/open-climate-compiler/setup.git
cd setup
bash setup.sh
```
Note that this script assumes that `bash`, `git` and the `boost` libraries are installed on your system.

## Requirements

- Bash
- Git
- Boost v1.63.0 or later
