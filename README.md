# Open Climate Compiler - Setup

This repository contains installation instructions to setup the Open Climate
Compiler (OCC) development environment, as well as a Dockerfile to set it up
in a Ubuntu 18.04 container.

## Installation instructions

This guide assumes that you are using Linux. Other operating systems have not
been tested yet but might be added in the future. For the sake of simplicity,
we will focus on setting up the development environment on Ubuntu. If you are
using another distribution, you might have to adapt some commands related to
package installation.

Note: If you follow this guide, you will install NVIDIA CUDA on your machine.
This is a required step to be able to generate CUDA C using the OCC compiler.
This will work even if you don't have a NVIDIA GPU, but be aware that you will
not be able to run any of the tests.

### Installing the required packages

The OCC setup depends on a the following libraries and components:
- Boost v1.63.0 or later
- Google Test
- NVIDIA CUDA
- zlib
- libtinfo
- libxml2
- Ninja
- CMake v3.12 or later

On Ubuntu 18.04, run the following commands to make sure that all the required
dependencies are installed:
```sh
sudo apt-get update
sudo apt-get install -y build-essential ninja-build curl git grep libboost-all-dev libgtest-dev openssh-client python3.7 python3-setuptools python3-pip zlib1g-dev libtinfo-dev libxml2-dev nvidia-cuda-dev nvidia-cuda-toolkit
```
As we need a fairly recent version of CMake, we will use `pip` to install it:
```sh
sudo pip3 install cmake
```

### Directory structure

Now that all the required dependencies are installed, let us create a directory
where we will setup our development environment:
```sh
mkdir -p $OCC_ROOT
```
where `$OCC_ROOT` is the directory where you want to build and install the OCC.
Note that you will need at least 100GiB of free space for this setup to work.

Now let us create the OCC installation prefix. This directory will contain all
the built binaries and installed headers produced by the software packages we
will be compiling. I will call mine `prefix` and put it inside of `$OCC_PREFIX`.
```sh
export PREFIX=$OCC_ROOT/prefix
mkdir -p $OCC_ROOT/prefix
```
The first line exports an environment variable that will come in handy later on.

### Building LLVM, Clang and MLIR

We first need to get LLVM and MLIR from the OCC Gitlab repositories.
```sh
cd $OCC_ROOT
git clone https://gitlab.com/open-climate-compiler/llvm-project.git llvm-project
git clone https://gitlab.com/open-climate-compiler/stencil-ir.git llvm-project/llvm/projects/mlir
```
We then proceed to build and install LLVM, Clang and MLIR. Note that the build
process will require some time and consume a significant amount of memory. You
can adjust the `LLVM_PARALLEL_LINK_JOBS` and the number of building threads to
fit your machine's capabilities. The build uses `gold` to link the LLVM
libraries and binaries, as it is much more efficient than the default system
`ld` in most cases. If you have `llvm` installed on your system, you can also
use `lld`.
```sh
cd $OCC_ROOT/llvm-project
mkdir build && cd build
cmake -G Ninja ../llvm -DLLVM_BUILD_EXAMPLES=OFF -DLLVM_TARGETS_TO_BUILD="host;NVPTX" -DCMAKE_INSTALL_PREFIX=$PREFIX -DLLVM_ENABLE_PROJECTS='clang' -DLLVM_OPTIMIZED_TABLEGEN=ON -DMLIR_CUDA_RUNNER_ENABLED=ON -DCMAKE_CUDA_COMPILER=`which nvcc` -DLLVM_USE_LINKER=gold -DLLVM_PARALLEL_LINK_JOBS=1
ninja -j$(nproc) install
```

After this long building process, you should have LLVM, Clang and MLIR installed
in `$PREFIX`. Let us export a few additional environment variables to help
subsequent builds to find the newly installed libraries, headers and binaries.
```sh
export LIBRARY_PATH=${PREFIX}/lib    # Compiler library search path
export LD_LIBRARY_PATH=${PREFIX}/lib # Dynamic library search path
export CPATH=${PREFIX}/include       # Compiler header search path
export PATH=${PREFIX}/bin:${PATH}    # Binary search path
```

### Building Dawn and GTClang

To build Dawn, we need to build Protobuf v3.4.0
```sh
cd $OCC_ROOT
git clone --branch v3.4.0 --depth 1 https://github.com/protocolbuffers/protobuf.git protobuf
cd protobuf
mkdir build && cd build
cmake -G Ninja ../cmake -Dprotobuf_BUILD_TESTS=OFF -Dprotobuf_BUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=$PREFIX
ninja -j$(nproc) install
```
and to build GTClang, we need to build GridTools v1.0.3
```sh
cd $OCC_ROOT
git clone --branch v1.0.3 --depth 1 https://github.com/GridTools/gridtools.git gridtools
cd gridtools
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$PREFIX -DGT_INSTALL_EXAMPLES=OFF -DBUILD_TESTING=OFF
make -j$(nproc) install
```

Next, we fetch Dawn and GTClang from the corresponding OCC Gitlab repository,
build them and install them to `$PREFIX`.
```sh
cd $OCC_ROOT
git clone https://gitlab.com/open-climate-compiler/dawn.git dawn
cd dawn/dawn
mkdir build && cd build
cmake -G Ninja .. -DProtobuf_DIR=$PREFIX/lib/cmake/protobuf -DCMAKE_INSTALL_PREFIX=$PREFIX
ninja -j$(nproc) install
cd $OCC_ROOT/dawn/gtclang
mkdir build && cd build
cmake -G Ninja .. -Ddawn_DIR=$PREFIX/cmake -DLLVM_ROOT=$PREFIX -DCMAKE_INSTALL_PREFIX=$PREFIX -DGTCLANG_BUILD_GT_CPU_EXAMPLES=OFF -DGTCLANG_BUILD_GT_GPU_EXAMPLES=OFF -DCTEST_CUDA_SUBMIT=OFF -DGTCLANG_BUILD_CUDA_EXAMPLES=ON -DCUDA_ARCH=sm_35 -DGridTools_DIR=$PREFIX/lib/cmake
CPATH=/usr/lib/gcc/x86_64-linux-gnu/`gcc --version | grep ^gcc | sed 's/^.* //g'`/include:$CPATH ninja -j$(nproc) install
```

### Running the tests

If you have an NVIDIA GPU and your driver is configured to accept CUDA programs,
you can run the OCC tests together with the GTClang unit tests by running
```sh
CPATH=/usr/lib/gcc/x86_64-linux-gnu/`gcc --version | grep ^gcc | sed 's/^.* //g'`/include:$CPATH ctest
```
inside of the `dawn/gtclang/build` directory.
