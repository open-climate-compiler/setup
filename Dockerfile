FROM ubuntu:18.04
LABEL maintainer="Jean-Michel Gorius <jean-michel.gorius@ens-rennes.fr>"
LABEL description="Open Climate Compiler (OCC) development environment"
RUN apt-get update && \
    apt-get install -y \
        build-essential \
	ninja-build \
	curl \
	git \
	grep \
	libboost-all-dev \
	libgtest-dev \
	openssh-client \
	python3.7 \
	python3-setuptools \
	python3-pip \
	sudo \
	zlib1g-dev \
	libtinfo-dev \
	libxml2-dev \
	nvidia-cuda-dev \
	nvidia-cuda-toolkit
RUN pip3 install cmake
RUN useradd -m -s /bin/bash occ-user && echo "occ-user:occ" | chpasswd && \
    adduser occ-user sudo
USER occ-user
RUN touch /home/occ-user/.sudo_as_admin_successful
ENV PREFIX /home/occ-user/prefix
RUN mkdir -p $PREFIX
ENV OCC_ROOT /home/occ-user/occ
RUN mkdir -p $OCC_ROOT
WORKDIR ${OCC_ROOT}
RUN git clone --progress --verbose https://gitlab.com/open-climate-compiler/llvm-project.git llvm-project
RUN git clone --progress --verbose https://gitlab.com/open-climate-compiler/stencil-ir.git llvm-project/llvm/projects/mlir
WORKDIR llvm-project
RUN mkdir build
WORKDIR build
RUN cmake -G Ninja ../llvm -DLLVM_BUILD_EXAMPLES=OFF -DLLVM_TARGETS_TO_BUILD="host;NVPTX;AMDGPU" -DCMAKE_INSTALL_PREFIX=$PREFIX -DLLVM_ENABLE_PROJECTS='clang' -DLLVM_OPTIMIZED_TABLEGEN=ON -DMLIR_CUDA_RUNNER_ENABLED=ON -DCMAKE_CUDA_COMPILER=`which nvcc` -DLLVM_USE_LINKER=gold -DLLVM_PARALLEL_LINK_JOBS=$(nproc)
RUN ninja -j$(nproc) install
ENV LIBRARY_PATH ${PREFIX}/lib
ENV CPATH ${PREFIX}/include
ENV PATH ${PREFIX}/bin:${PATH}
ENV LD_LIBRARY_PATH ${PREFIX}/lib
WORKDIR ${OCC_ROOT}
RUN git clone --progress --verbose --branch v3.4.0 --depth 1 https://github.com/protocolbuffers/protobuf.git protobuf
WORKDIR protobuf
RUN mkdir build
WORKDIR build
RUN cmake -G Ninja ../cmake -Dprotobuf_BUILD_TESTS=OFF -Dprotobuf_BUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=$PREFIX
RUN ninja -j$(nproc) install
WORKDIR ${OCC_ROOT}
RUN git clone --progress --verbose https://gitlab.com/open-climate-compiler/dawn.git dawn
WORKDIR dawn/dawn
RUN mkdir build
WORKDIR build
RUN cmake -G Ninja .. -DProtobuf_DIR=$PREFIX/lib/cmake/protobuf -DCMAKE_INSTALL_PREFIX=$PREFIX
RUN ninja -j$(nproc) install
WORKDIR ${OCC_ROOT}
RUN git clone --progress --verbose --branch v1.0.3 --depth 1 https://github.com/GridTools/gridtools.git gridtools
WORKDIR gridtools
RUN mkdir build
WORKDIR build
RUN cmake .. -DCMAKE_INSTALL_PREFIX=$PREFIX -DGT_INSTALL_EXAMPLES=OFF -DBUILD_TESTING=OFF
RUN make -j$(nproc) install
WORKDIR ${OCC_ROOT}/dawn/gtclang
RUN mkdir build
WORKDIR build
RUN cmake -G Ninja .. -Ddawn_DIR=$PREFIX/cmake -DLLVM_ROOT=$PREFIX -DCMAKE_INSTALL_PREFIX=$PREFIX -DGTCLANG_BUILD_GT_CPU_EXAMPLES=OFF -DGTCLANG_BUILD_GT_GPU_EXAMPLES=OFF -DCTEST_CUDA_SUBMIT=OFF -DGTCLANG_BUILD_CUDA_EXAMPLES=ON -DCUDA_ARCH=sm_35 -DGridTools_DIR=$PREFIX/lib/cmake
RUN CPATH=/usr/lib/gcc/x86_64-linux-gnu/`gcc --version | grep ^gcc | sed 's/^.* //g'`/include:$CPATH ninja -j$(nproc) install
RUN ctest
