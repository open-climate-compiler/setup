FROM ubuntu:19.04
LABEL maintainer="Jean-Michel Gorius <jean-michel.gorius@ens-rennes.fr>"
LABEL description="Open Climate Compiler (OCC) development environment"
RUN apt-get update && \
    apt-get install -y \
        build-essential \
        cmake \
        curl \
        git \
        grep \
        libboost-all-dev \
        libgtest-dev \
        ninja-build \
        openssh-client \
        python3.7 \
        python3-setuptools \
        sudo \
        vim
RUN useradd -m -s /bin/bash occ-user && echo "occ-user:docker" | chpasswd && adduser occ-user sudo
WORKDIR /home/occ-user
USER occ-user
RUN git clone https://gitlab.com/open-climate-compiler/setup.git && chmod +x setup/setup.sh
RUN echo 'Y\n\nY\n' | ./setup/setup.sh occ
RUN touch .sudo_as_admin_successful
