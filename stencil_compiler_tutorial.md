# Writing a stencil compiler using MLIR

## Introduction

This tutorial will guide you in the process of writing your own domain-specific compiler for stencil programs. It is based on the Open Climate Compiler (OCC) infrastructure, which is available [on Gitlab](https://gitlab.com/open-climate-compiler).

In the following, we will assume that you have a working OCC setup. If you want to setup the OCC infrastructure, you can follow the [online setup instructions](https://gitlab.com/open-climate-compiler/setup/blob/master/README.md) [^1]. Throughout the tutorial, `$OCC_ROOT` will refer to the root directory of the OCC development environment.

## Motivating example

In this tutorial, we will build our very own domain-specific compiler for stencil programs. We will learn how to implement a dialect using MLIR, how to define custom types and custom operations, write a lowering pass as well as a translation pass. In the end, we will have an end-to-end frontend-agnostic stencil compiler capable of compiling quite complex stencil programs targeting CPUs as well as GPUs.

The following program is what our input will look like. This program applies the Laplace operator defined by
$$
\mathrm{lap}(\mathrm{in})=-4\times\mathrm{in}+\mathrm{in}[i - 1]+\mathrm{in}[i + 1]+\mathrm{in}[j - 1]+\mathrm{in}[j + 1].
$$
We will use this example throughout the tutorial to introduce the various concepts that appear when designing a new intermediate representation (IR) using MLIR.

```mlir
func @lap(%in : !sten.view<?x?x?xf64>) -> f64
  attributes { sten.function } {
	%0 = sten.access %in[-1, 0, 0] : !sten.view<?x?x?xf64>
	%1 = sten.access %in[ 1, 0, 0] : !sten.view<?x?x?xf64>
	%2 = sten.access %in[ 0, 1, 0] : !sten.view<?x?x?xf64>
	%3 = sten.access %in[ 0,-1, 0] : !sten.view<?x?x?xf64>
	%4 = sten.access %in[ 0, 0, 0] : !sten.view<?x?x?xf64>
	%5 = addf %0, %1 : f64
	%6 = addf %2, %3 : f64
	%7 = addf %5, %6 : f64
	%8 = constant -4.0 : f64
	%9 = mulf %4, %8 : f64
	%10 = addf %9, %7 : f64
	return %10 : f64
}

func @lap_stencil(%in: !sten.field<?x?x?xf64>, %out: !sten.field<12x12x16xf64>)
  attributes { sten.program } {
	%0 = sten.load %in : (!sten.field<?x?x?xf64>) -> !sten.view<?x?x?xf64>
	%1 = sten.apply @lap(%0) : (!sten.view<?x?x?xf64>) -> !sten.view<?x?x?xf64>
	%2 = sten.apply @lap(%1) : (!sten.view<?x?x?xf64>) -> !sten.view<?x?x?xf64>
	sten.store %out, %2 : !sten.field<12x12x16xf64>, !sten.view<?x?x?xf64>
	return
}
```

The following section breaks down the essential parts of this example program.

### Example program breakdown

MLIR is organized around the notion of *dialects*. A dialect defines a unique *namespace* in which we can define new [*operations*](https://github.com/tensorflow/mlir/blob/master/g3doc/LangRef.md#operations), [*types*](https://github.com/tensorflow/mlir/blob/master/g3doc/LangRef.md#type-system) and [*attributes*](https://github.com/tensorflow/mlir/blob/master/g3doc/LangRef.md#attributes). We will work on a dialect called Sten, which is a shorthand for *Stencil*. Even though this is a simple stencil dialect, it will be able to cover many cases for which stencils are used. Moreover, as we will see, it is very easy to extend and add new features to such a dialect.

You will notice that some parts of the program contain names prefixed with `!sten`. These are custom types defined in the Sten dialect. We will introduce two custom types in our dialect, namely a `field` type and a `view` type. Conceptually, a *field* represents an $n$-dimensional array of values, and a *view* is a reference to parts of a field. The basic syntax for fields is as follows.

```ebnf
field-type ::= `!sten.field<` dimension-list `x` element-type `>`
dimension-list ::= dimension (`x` dimension)*
dimension ::= integer-literal | `?`
element-type ::= `f64` | `i64`
```

The `?` appearing in the dimension list signifies that the dimension is not known at compile-time. The syntax for views follows the same rules.

In the above program, operations prefixed with `sten.` are part of the Sten dialect namespace. Other operations, such as `addf`, `mulf` and `constant` are part of the core MLIR dialect called "StandardOps". We will define the following operations in our dialect:

- `sten.access` accesses a view at the given offset relative to the current position. The general syntax for `sten.access` is

  ```ebnf
  access-op ::= `sten.access` ssa-id `[` offset-list `] :` element-type
  offset-list ::= integer-literal (`,` integer-literal)*
  ```

  Refer to [this page](https://github.com/tensorflow/mlir/blob/master/g3doc/LangRef.md#common-syntax) for more information on `ssa-id` and `integer-literal`. The length of the offset list has to match the number of dimensions of the given view.

- `sten.load` creates a view from a field. The view's element type will be the same as the underlying field's element type. The dimensions of the view are unknown by default but could be inferred by a compiler pass. The general syntax is

  ```ebnf
  load-op ::= `sten.load` source-field `:` view-type
  source-field ::= ssa-id
  ```

- `sten.store` stores a view to a field. Conceptually, this operation copies the contents of the view to the given field. However, this copy can often be avoided and simplified by the compiler. The syntax of this operation is given by

  ```ebnf
  store-op ::= `sten.store` target-field `,` source-view `:` field-type
  target-field ::= ssa-id
  source-view ::= ssa-id
  ```

  To simplify things a little bit, we will require the size of the target field to be known at compile time.

- `sten.apply` applies a `stencil.function` to the given view. The semantics of this operation is very similar to a CUDA kernel call in the sense that the given function is run on all the elements of the view the stencil function is applied to. Its syntax is as follows.

  ```ebnf
  apply-op ::= `sten.apply` symbol-ref-id `(` argument-list `) :` function-type
  ```

  For more details on `symbol-ref-id`, `argument-list` and `function-type`, see the [online MLIR language reference](https://github.com/tensorflow/mlir/blob/master/g3doc/LangRef.md).

## Setting up the project

Now that we have a better understanding of what our dialect will look like, let's take a look at how to setup the directory structure and basic files required to create a new dialect in MLIR. Note that the steps exposed below do not describe *the right way* to do things, but rather give an overview of how the existing MLIR dialects are setup inside the project at the time of this writing.

### Overview of the MLIR directory structure

The following gives a brief overview of the directory structure of the MLIR project as can be found in the [`open-climate-compiler/stencil-ir`](https://gitlab.com/open-climate-compiler/stencil-ir) repository.

- `bindings/python` contains Python bindings for MLIR. These enable direct interfacing of MLIR with Python, which can be especially useful for people designing domain-specific frontends.
- `examples` contain some example dialects used in tutorials.
- `g3doc` contains a lot of useful documentation about MLIR, ranging from a language reference to an overview of the pattern rewriting infrastructure. I strongly advise people interested in MLIR to take a quick look at what can be found in this folder.
- `include` contains two folders, namely `mlir` and `mlir-c`. The latter contains the main header file defining the C interface to MLIR. We will mainly focus on the `mlir` folder, which contains all the MLIR header files. Inside this folder, you will find the following:
  - `Analysis` contains some compiler analysis passes such as SSA dominance information computation.
  - `Conversion` contains one subfolder for each of the available lowering passes. This is where we will put our own lowering passes.
  - `Dialect` is the home of all the MLIR dialects. Each dialect lives in its own subfolder. This is where we will create most of our header files.
  - `EDSC` contains headers related to the declarative MLIR builder interface. This is still a work-in-progress and we will not cover it in this tutorial. However, if you are interested, you can take a look at [the official documentation](https://github.com/tensorflow/mlir/blob/master/g3doc/EDSC.md).
  - `ExecutionEngine` is where all the JIT-related headers are stored.
  - `IR` contains the core implementation of MLIR. This is where most of the functionalities we will use to develop our dialect are stored. I strongly advise you to have a look at those files when you are searching for documentation about a specific class or function.
  - `Pass` is where all the pass management logic is defined.
  - `Quantizer` contains files related to floating-point quantization.
  - `Support` contains many useful functions that make our lives easier when implementing dialects, operations, types and attributes.
  - `TableGen` defines the syntax and operation of the MLIR TableGen tool. This tool is very similar to `llvm-tblgen`. You can find more information about TableGen on [the LLVM website](https://llvm.org/docs/TableGen/).
  - `Target` is where the translation targets are defined. These files define the conversion steps executed by the `mlir-translate` tool to go from MLIR IR to a target file. The LLVM IR backend and the OCC custom CUDA C backend live there.
  - `Transforms` contains utilities related to code transformations. This is where the core interfaces to common transformation passes are defined.
- `lib` contains the actual MLIR source files. The directory structure nearly follows the one described above for the `include/mlir` folder.
- `test` contains FileCheck tests. MLIR makes use of LLVM FileCheck to easily write test cases. These tests are basically automatic `diff`s that report an error if the expected output and the actual output don't match. To execute test cases from this folder, simply build the `check-mlir` target inside of the LLVM build directory.
- `tools` is where the source files for the MLIR tools (e.g. `mlir-opt` and `mlir-translate`) are stored.
- `unittests` contains unit tests. Who would have guessed?
- `utils` contains miscellaneous files mostly related to MLIR syntax highlighting integration for various editors. Check this directory to see if your favorite text editor is supported.

### Creating the base files for the Sten dialect

After this brief tour of the MLIR codebase, let us create the basic files for our new Sten dialect. To make our lives easier, let's create a symlink to the MLIR subfolder inside of the `$OCC_ROOT` directory.

```sh
ln -sf $OCC_ROOT/llvm-project/llvm/projects/mlir $OCC_ROOT/mlir
```

#### Header files and TableGen files

Now let's create the Sten dialect folder in the `mlir/include/mlir/Dialect` directory.

```sh
cd $OCC_ROOT/mlir/include/mlir/Dialect
mkdir Sten
```

We will now populate this folder with some base files. The following section describes each of these files.

##### `StenBase.td`

This file will contain base definitions to create Sten operations using TableGen. Note the `.td` extension for TableGen files. This file will also contain TableGen type definitions as well as some utilities.

```tablegen
// Dialect/Sten/StenBase.td

#ifndef STEN_BASE
#define STEN_BASE

#ifndef OP_BASE
include "mlir/IR/OpBase.td"
#endif // OP_BASE

//===----------------------------------------------------------------------===//
// Sten dialect definition
//===----------------------------------------------------------------------===//

def Sten_Dialect : Dialect {
    let name = "sten";

    let description = [{
        A simple stencil dialect in MLIR.
    }];

    let cppNamespace = "sten";
}

//===----------------------------------------------------------------------===//
// Sten op definition
//===----------------------------------------------------------------------===//

// Base class for all Sten ops.
class Sten_Op<string mnemonic, list<OpTrait> traits = []> :
    Op<Sten_Dialect, mnemonic, traits> {

    // For each Sten op, the following static functions need to be defined in
    // StenOps.cpp:
    //
    // * static ParseResult parse<op-c++-class-name>(OpAsmParser &parser,
    //                                               OperationState &state);
    // * static void print(OpAsmPrinter &p, <op-c++-class-name> op)
    // * static LogicalResult verify(<op-c++-class-name> op)
    let parser = [{ return ::parse$cppClass(parser, result); }];
    let printer = [{ ::print(*this, p); }];
    let verifier = [{ return ::verify(*this); }];
}

#endif // STEN_BASE
```

##### `StenDialect.h`

This file contains the declaration of our Sten dialect.

```c++
// Dialect/Sten/StenDialect.h

#ifndef MLIR_DIALECT_STEN_STENDIALECT_H
#define MLIR_DIALECT_STEN_STENDIALECT_H

#include "mlir/IR/Dialect.h"

namespace mlir {
namespace sten {

class StenDialect : public Dialect {
public:
  explicit StenDialect(MLIRContext *context);

  /// Returns the prefix used in the textual IR to refer to Sten operations
  static StringRef getDialectNamespace() { return "sten"; }

  /// Parses a type registered to this dialect
  Type parseType(DialectAsmParser &parser) const override;

  /// Prints a type registered to this dialect
  void printType(Type type, DialectAsmPrinter &os) const override;
};

} // namespace sten
} // namespace mlir

#endif // MLIR_DIALECT_STEN_STENDIALECT_H
```

##### `StenTypes.h`

This is where we will define our custom dialect types.

```c++
// Dialect/Sten/StenTypes.h

#ifndef MLIR_DIALECT_STEN_STENTYPES_H
#define MLIR_DIALECT_STEN_STENTYPES_H

#include "mlir/IR/TypeSupport.h"
#include "mlir/IR/Types.h"

namespace mlir {
namespace sten {}
} // namespace mlir

#endif // MLIR_DIALECT_STEN_STENTYPES_H
```

##### `StenOps.td`

This file will contain definitions for operations using the declarative TableGen syntax. TableGen will generate a C++ implementation file alongside class declarations to include in `StenOps.h`.

```tablegen
// Dialect/Sten/StenOps.td

#ifndef STEN_OPS
#define STEN_OPS

#ifndef STEN_BASE
include "mlir/Dialect/Sten/StenBase.td"
#endif // STEN_BASE

#endif // STEN_OPS
```

##### `StenOps.h`

This file will contain our custom operations. It will include the code generated by TableGen for the operations defined in `StenOps.td` as well as custom operations directly defined in C++.

```c++
// Dialect/Sten/StenOps.h

#ifndef MLIR_DIALECT_STEN_STENOPS_H
#define MLIR_DIALECT_STEN_STENOPS_H

#include "mlir/Dialect/Sten/StenTypes.h"
#include "mlir/IR/Function.h"

namespace mlir {
namespace sten {

/// Retrieve the class declarations generated by TableGen
#define GET_OP_CLASSES
#include "mlir/Dialect/Sten/StenOps.h.inc"

} // namespace stencil
} // namespace mlir

#endif // MLIR_DIALECT_STEN_STENOPS_H
```

##### `CMakeLists.txt`

The last step is to instruct TableGen to generate files for our custom operations.

```cmake
# Dialect/Sten/CMakeLists.txt

set(LLVM_TARGET_DEFINITIONS StenOps.td)

# Generate the class interfaces
mlir_tablegen(StenOps.h.inc -gen-op-decls)
# Generate the actual implementation
mlir_tablegen(StenOps.cpp.inc -gen-op-defs)

add_public_tablegen_target(MLIRStenOpsIncGen)
```

#### Source files

Let us now head to the `mlir/lib/Dialect` folder.

```sh
cd $OCC_ROOT/mlir/lib/Dialect
mkdir Sten
```

We will populate this folder with some source files which will contain the actual implementation of our Sten dialect.

##### `DialectRegistration.cpp`

This file is used to statically register the Sten dialect.

```c++
// Dialect/Sten/DialectRegistration.cpp

#include "mlir/Dialect/Sten/StenDialect.h"

// Static registration of the Sten dialect
static mlir::DialectRegistration<mlir::sten::StenDialect> stenDialect;
```

##### `StenTypes.cpp`

This is where we will implement our custom types.

```c++
// Dialect/Sten/StenTypes.cpp

#include "mlir/Dialect/Sten/StenTypes.h"

using namespace mlir;
using namespace mlir::sten;

// Custom types go here
```

##### `StenOps.cpp`

This file is where we will define all the functions related to our custom Sten operations. Most of them will be related to printing and parsing the operation. The `StenOps.cpp` will also include the operation implementation file generated by TableGen.

```c++
// Dialect/Sten/StenOps.cpp

#include "mlir/Dialect/Sten/StenOps.h"

#include "mlir/Dialect/Sten/StenDialect.h"
#include "mlir/Dialect/Sten/StenTypes.h"
#include "mlir/IR/Attributes.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/Function.h"
#include "mlir/IR/OpImplementation.h"

using namespace mlir;

// Custom operations go here

namespace mlir {
namespace sten {
#define GET_OP_CLASSES
#include "mlir/Dialect/Sten/StenOps.cpp.inc"
} // namespace stencil
} // namespace mlir
```

##### `StenDialect.cpp`

This file will contain the definition of dialect methods, most notably custom type printing and parsing.

```c++
// Dialect/Sten/StenDialect.cpp

#include "mlir/Dialect/Sten/StenDialect.h"
#include "mlir/Dialect/Sten/StenOps.h"
#include "mlir/Dialect/Sten/StenTypes.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/DialectImplementation.h"

using namespace mlir;
using namespace mlir::sten;

//===----------------------------------------------------------------------===//
// Sten Dialect
//===----------------------------------------------------------------------===//

StenDialect::StenDialect(mlir::MLIRContext *context)
    : Dialect(getDialectNamespace(), context) {
  // Allow Sten operations to exist in their generic form
  allowUnknownOperations();
}

//===----------------------------------------------------------------------===//
// Type Parsing
//===----------------------------------------------------------------------===//

Type StenDialect::parseType(DialectAsmParser &parser) const {
  parser.emitError(parser.getNameLoc(), "unknown Sten type: ")
      << parser.getFullSymbolSpec();
  return Type();
}

//===----------------------------------------------------------------------===//
// Type Printing
//===----------------------------------------------------------------------===//

void StenDialect::printType(Type type, DialectAsmPrinter &printer) const {
  switch (type.getKind()) {
  default:
    llvm_unreachable("unhandled Sten type");
  }
}
```

##### `CMakeLists.txt`

Here we will define the Sten library as well as its dependencies.

```cmake
# Dialect/Sten/CMakeLists.txt

add_llvm_library(MLIRSten
        DialectRegistration.cpp
        StenDialect.cpp
        StenOps.cpp
        StenTypes.cpp

        ADDITIONAL_HEADER_DIRS
        ${MLIR_MAIN_INCLUDE_DIR}/mlir/Dialect/Sten
        )

# Make sure that the TableGen generated files are up-to-date
add_dependencies(MLIRSten
        MLIRStenOpsIncGen)

target_link_libraries(MLIRSten
        MLIRIR
        MLIRParser
        MLIRSupport)
```

### Integration in the MLIR build process

The `mlir/include/mlir/Dialect/Sten` folder should now contain the following files:

- `CMakeLists.txt`
- `StenDialect.h`
- `StenBase.td`
- `StenOps.h`
- `StenOps.td`
- `StenTypes.h`

and the `mlir/lib/Dialect/Sten` folder should contain

- `CMakeLists.txt`
- `DialectRegistration.cpp`
- `StenDialect.cpp`
- `StenOps.cpp`
- `StenTypes.cpp`

The final step is to integrate our newly created dialect in the MLIR build process. To achieve this, we simply need to add the following line to `$OCC_ROOT/mlir/include/mlir/Dialect/CMakeLists.txt` and `$OCC_ROOT/mlir/lib/Dialect`:

```cmake
add_subdirectory(Sten)
```

Finally, as we will be creating custom types, we need to reserve parts of the MLIR identifier space for the Sten dialect. This is achieved by adding

```c
DEFINE_SYM_KIND_RANGE(STEN) // Sten dialect
```

to `mlir/include/mlir/IR/DialectSymbolRegistry.def`. To test that everything is working properly, let us build MLIR. Go to `$OCC_ROOT/llvm-project/build` and run

```sh
cmake --build . --target install
```

Congratulations! You have created your first MLIR dialect!

## Creating your first type

In its current state, our Sten dialect is not very exciting. It is just an empty namespace floating around in the realm of MLIR dialects. Let's make this a bit more interesting by introducing our first custom type: fields.  But first, we will take a quick look the typical dialect development workflow in MLIR.

### Dialect development workflow

MLIR strongly encourages test-driven development. We will not have time to cover all the possible ways to write tests in MLIR but we will focus on a small part of them that will help us test our dialect implementation. In particular, we will have a look at how to create FileCheck tests in the `mlir/test` directory.

First, let us create a folder for tests related to the Sten dialect as well as an MLIR file called `ops.mlir`:

```sh
cd $OCC_ROOT/mlir/test/Dialect
mkdir Sten
touch Sten/ops.mlir
```

The `ops.mlir` file will serve as a base file for testing our custom type and operation printers and parsers. We can fill it with the example given at the beginning of this tutorial:

```mlir
func @lap(%in : !sten.view<?x?x?xf64>) -> f64
  attributes { sten.function } {
	%0 = sten.access %in[-1, 0, 0] : !sten.view<?x?x?xf64>
	%1 = sten.access %in[ 1, 0, 0] : !sten.view<?x?x?xf64>
	%2 = sten.access %in[ 0, 1, 0] : !sten.view<?x?x?xf64>
	%3 = sten.access %in[ 0,-1, 0] : !sten.view<?x?x?xf64>
	%4 = sten.access %in[ 0, 0, 0] : !sten.view<?x?x?xf64>
	%5 = addf %0, %1 : f64
	%6 = addf %2, %3 : f64
	%7 = addf %5, %6 : f64
	%8 = constant -4.0 : f64
	%9 = mulf %4, %8 : f64
	%10 = addf %9, %7 : f64
	return %10 : f64
}

func @lap_stencil(%in: !sten.field<?x?x?xf64>, %out: !sten.field<12x12x16xf64>)
  attributes { sten.program } {
	%0 = sten.load %in : (!sten.field<?x?x?xf64>) -> !sten.view<?x?x?xf64>
	%1 = sten.apply @lap(%0) : (!sten.view<?x?x?xf64>) -> !sten.view<?x?x?xf64>
	%2 = sten.apply @lap(%1) : (!sten.view<?x?x?xf64>) -> !sten.view<?x?x?xf64>
	sten.store %out, %2 : !sten.field<12x12x16xf64>, !sten.view<?x?x?xf64>
	return
}
```

Now let us build the `check-mlir` target. The `ops.mlir` file will be automatically found by MLIR and processed for testing.

```sh
cd $OCC_ROOT/llvm-project/build
cmake --build . --target check-mlir
```

You will notice two different kinds of errors. The first one reads

```sh
UNRESOLVED: MLIR :: Dialect/Sten/ops.mlir (191 of 342)
******************** TEST 'MLIR :: Dialect/Sten/ops.mlir' FAILED ***************
Test has no run line!
********************
```

and the other ones look like

```sh
FAIL: MLIR :: mlir-cuda-runner/all-reduce-op.mlir (340 of 342)
******************** TEST 'MLIR :: mlir-cuda-runner/all-reduce-op.mlir' FAILED ***********
...
Command Output (stderr):
--
CUDA failed with 700 in StreamSync
.../all-reduce-op.mlir:3:11: error: CHECK: expected string not found in input
// CHECK: [5.356000e+03, 5.356000e+03, {{.*}}, 5.356000e+03, 5.356000e+03]
          ^
<stdin>:1:1: note: scanning from here
[1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00]
^
<stdin>:1:125: note: possible intended match here
[1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00, 1.230000e+00]
                                                                                                                            ^
--
```

The latter ones show how a failing test looks like. You should have three different failing tests, all related to `mlir-cuda-runner`. At the time of this writing, there is ongoing work in the MLIR development team to fix these test cases. We will just disable them as we don't need the `mlir-cuda-runner` for now. To disable the `mlir-cuda-runner` test cases, simply rename them and give them a different extension (there are located in the `mlir/test/mlir-cuda-runner` directory). I renamed mine to

```
all-reduce-op.mlir.old
all-reduce-region.mlir.old
gpu-to-cubin.mlir.old
```

Now run

```sh
cmake --build . --target check-mlir
```

again. You should only see the `Test has no run line!` error. This error is caused by the LLVM FileCheck tool, who is looking for a so-called *run line* at the very top of our `ops.mlir` test file. A run line is a comment starting with `RUN: ` and followed by the command to execute for testing. Let's add a run line to our test file:

```mlir
// RUN: mlir-opt %s | FileCheck %s
```

The `%s` refers to the current file name. This run line will launch `mlir-opt` on the `ops.mlir` file and pipe its output to the FileCheck utility. If you build the `check-mlir` target again, you should now see the following error:

```sh
FAIL: MLIR :: Dialect/Sten/ops.mlir (193 of 339)
******************** TEST 'MLIR :: Dialect/Sten/ops.mlir' FAILED ***************
...
Command Output (stderr):
--
error: no check strings found with prefix 'CHECK:'
.../ops.mlir:5:7: error: custom op 'sten.access' is unknown
        %0 = sten.access %in[-1, 0, 0] : f64
             ^
```

We will talk about the `error: no check strings found with prefix 'CHECK:'` line later in this tutorial. The second error line (`error: custom op 'sten.access' is unknown`) is the one of interest to us. It tells us that `mlir-opt` does not know the `sten.access` operation. This is fine, as we haven't even defined it yet. But what happens if we change

```mlir
%0 = sten.access %in[-1, 0, 0] : !sten.view<?x?x?xf64>
```

to

```mlir
%0 = "sten.access"(%in) {offset = [-1, 0, 0]} : (!sten.view<?x?x?xf64>) -> f64
```

in the `@lap` function? Now the error happens on the next line! MLIR treated this last line as a valid IR line and processed it without any problem. This is one of the major strength of MLIR when it comes to IR prototyping and design. Each operation has a so-called *generic form* which is processed using a default printer and parser. This means that we can define our entire test file using the generic syntax and iteratively replace pieces of it with the pretty-printed version of our custom operations.

Here is the generic form of our test file:

```mlir
// RUN: mlir-opt %s | FileCheck %s

func @lap(%in : !sten.view<?x?x?xf64>) -> f64
  attributes { sten.function } {
	%0 = "sten.access"(%in) {offset = [-1, 0, 0]} : (!sten.view<?x?x?xf64>) -> f64
	%1 = "sten.access"(%in) {offset = [ 1, 0, 0]} : (!sten.view<?x?x?xf64>) -> f64
	%2 = "sten.access"(%in) {offset = [ 0, 1, 0]} : (!sten.view<?x?x?xf64>) -> f64
	%3 = "sten.access"(%in) {offset = [ 0,-1, 0]} : (!sten.view<?x?x?xf64>) -> f64
	%4 = "sten.access"(%in) {offset = [ 0, 0, 0]} : (!sten.view<?x?x?xf64>) -> f64
	%5 = addf %0, %1 : f64
	%6 = addf %2, %3 : f64
	%7 = addf %5, %6 : f64
	%8 = constant -4.0 : f64
	%9 = mulf %4, %8 : f64
	%10 = addf %9, %7 : f64
	return %10 : f64
}

func @lap_stencil(%in: !sten.field<?x?x?xf64>, %out: !sten.field<12x12x16xf64>)
  attributes { sten.program } {
	%0 = "sten.load"(%in) : (!sten.field<?x?x?xf64>) -> !sten.view<?x?x?xf64>
	%1 = "sten.apply"(%0) { callee = @lap } : (!sten.view<?x?x?xf64>) -> !sten.view<?x?x?xf64>
	%2 = "sten.apply"(%1) { callee = @lap } : (!sten.view<?x?x?xf64>) -> !sten.view<?x?x?xf64>
	"sten.store"(%out, %2) : (!sten.field<12x12x16xf64>, !sten.view<?x?x?xf64>) -> ()
	return
}
```

For more information on the generic form of operations, you can have a look at [the MLIR language reference](https://github.com/tensorflow/mlir/blob/master/g3doc/LangRef.md#operations).

What happens if we change

```mlir
%0 = "sten.load"(%in) : (!sten.field<?x?x?xf64>) -> !sten.view<?x?x?xf64>
```

to

```mlir
%0 = "sten.load"(%in) : (!sten.field<?x?x?xf64>) -> !sten.field<?x?x?xf64>
```

(note the different return type)? Building `check-mlir` fails during type checking! Even in their generic form, operations are still type-checked. This makes it very easy to write IR prototypes with a sound type interface.

#### Writing FileCheck tests

Let us now address the last error, `error: no check strings found with prefix 'CHECK:'`. This is FileCheck complaining that it did not find anything to do. By using comments starting with `CHECK:`, we can specify the expected output of the next lines. Here is an example of what a FileCheck test case could look like:

```mlir
// RUN: mlir-opt %s | FileCheck %s

func @lap(%in : !sten.view<?x?x?xf64>) -> f64
  attributes { sten.function } {
	%0 = "sten.access"(%in) {offset = [-1, 0, 0]} : (!sten.view<?x?x?xf64>) -> f64
	%1 = "sten.access"(%in) {offset = [ 1, 0, 0]} : (!sten.view<?x?x?xf64>) -> f64
	%2 = "sten.access"(%in) {offset = [ 0, 1, 0]} : (!sten.view<?x?x?xf64>) -> f64
	%3 = "sten.access"(%in) {offset = [ 0,-1, 0]} : (!sten.view<?x?x?xf64>) -> f64
	%4 = "sten.access"(%in) {offset = [ 0, 0, 0]} : (!sten.view<?x?x?xf64>) -> f64
	%5 = addf %0, %1 : f64
	%6 = addf %2, %3 : f64
	%7 = addf %5, %6 : f64
	%8 = constant -4.0 : f64
	%9 = mulf %4, %8 : f64
	%10 = addf %9, %7 : f64
	return %10 : f64
}

// CHECK-LABEL: func @lap(%{{.*}}: !sten.view<?x?x?xf64>) -> f64
//  CHECK-NEXT: attributes {sten.function} {
//  CHECK-NEXT: %{{.*}} = "sten.access"(%{{.*}}) {offset = [-1, 0, 0]} : (!sten.view<?x?x?xf64>) -> f64
//  CHECK-NEXT: %{{.*}} = "sten.access"(%{{.*}}) {offset = [1, 0, 0]} : (!sten.view<?x?x?xf64>) -> f64
//  CHECK-NEXT: %{{.*}} = "sten.access"(%{{.*}}) {offset = [0, 1, 0]} : (!sten.view<?x?x?xf64>) -> f64
//  CHECK-NEXT: %{{.*}} = "sten.access"(%{{.*}}) {offset = [0, -1, 0]} : (!sten.view<?x?x?xf64>) -> f64
//  CHECK-NEXT: %{{.*}} = "sten.access"(%{{.*}}) {offset = [0, 0, 0]} : (!sten.view<?x?x?xf64>) -> f64

func @lap_stencil(%in: !sten.field<?x?x?xf64>, %out: !sten.field<12x12x16xf64>)
  attributes { sten.program } {
	%0 = "sten.load"(%in) : (!sten.field<?x?x?xf64>) -> !sten.view<?x?x?xf64>
	%1 = "sten.apply"(%0) { callee = @lap } : (!sten.view<?x?x?xf64>) -> !sten.view<?x?x?xf64>
	%2 = "sten.apply"(%1) { callee = @lap } : (!sten.view<?x?x?xf64>) -> !sten.view<?x?x?xf64>
	"sten.store"(%out, %2) : (!sten.field<12x12x16xf64>, !sten.view<?x?x?xf64>) -> ()
	return
}

// CHECK-LABEL: func @lap_stencil(%{{.*}}: !sten.field<?x?x?xf64>, %{{.*}}: !sten.field<12x12x16xf64>)
//  CHECK-NEXT: attributes {sten.program}
//  CHECK-NEXT: %{{.*}} = "sten.load"(%{{.*}}) : (!sten.field<?x?x?xf64>) -> !sten.view<?x?x?xf64>
//  CHECK-NEXT: %{{.*}} = "sten.apply"(%{{.*}}) {callee = @lap} : (!sten.view<?x?x?xf64>) -> !sten.view<?x?x?xf64>
//  CHECK-NEXT: %{{.*}} = "sten.apply"(%{{.*}}) {callee = @lap} : (!sten.view<?x?x?xf64>) -> !sten.view<?x?x?xf64>
//  CHECK-NEXT: "sten.store"(%{{.*}}, %{{.*}}) : (!sten.field<12x12x16xf64>, !sten.view<?x?x?xf64>) -> ()
```

There is a lot more to writing test cases with FileCheck, but we  won't have time to cover all the details in this tutorial. For additional information, see [the LLVM documentation](https://llvm.org/docs/CommandGuide/FileCheck.html) as well as the files in the `mlir/test` folder.

### Defining the `field` type

After this short introduction to testing, let's go back to defining our dialect. We will start with types, as those will be needed to define operations that operate on them. We will start with the `sten.field` type. Remember that the general syntax of our `field` type is

```ebnf
field-type ::= `!sten.field<` dimension-list `x` element-type `>`
dimension-list ::= dimension (`x` dimension)*
dimension ::= integer-literal | `?`
element-type ::= `f64` | `i64`
```

To simplify things a little, let us restrict ourselves to 3D fields. The first question to ask ourselves when adding a new type to a dialect is "What are the parameters that uniquely define a given `field` type?". Since we choose to work only with 3D fields, a `field` is uniquely defined by the type of elements it contains and a triple representing its size in each dimension. We will use these parameters to describe our `field` type.

Let's start adding some code to the `StenTypes.h` file. The following code snippets only show the relevant parts of the file for the sake of readability.

```c++
// Inside the mlir::sten namespace

enum StenTypes {
  Field = Type::FIRST_STEN_TYPE,
  LAST_USED_STEN_TYPE = Field
};
```

We create an `enum` that will hold unique identifiers for each of our custom types. The `Type::FIRST_STEN_TYPE` symbol is defined by the line we added in the `DialectSymbolRegistry.def` file when we created the basic dialect setup. It is the start of a range of unique identifiers allocated by MLIR.  `LAST_USED_STEN_TYPE` is used to generate the next range of type identifiers.

```c++
/// The FieldTypeStorage structure contains the parameters we identified as uniquely
/// defining the `field` type.
struct FieldTypeStorage;
class FieldType : public Type::TypeBase<FieldType, Type, FieldTypeStorage> {
public:
  // Used for generic hooks in TypeBase.
  using Base::Base;

  /// Construction hook.
  ///
  /// Create a field type of given `shape` containing elements of type
  /// `elementType`.
  ///
  /// Note: `shape` must contain 3 elements, -1 being used to specify an unknown
  /// size.
  static FieldType get(MLIRContext *context, Type elementType,
                       ArrayRef<int64_t> shape);

  /// Used to implement LLVM-style casts.
  static bool kindof(unsigned kind) { return kind == StenTypes::Field; }

  /// Return the type of the field elements.
  Type getElementType();
  /// Return the shape of the field.
  ArrayRef<int64_t> getShape();
};
```

Now let's open the `StenTypes.cpp` file and start implementing our field type.

```c++
struct mlir::sten::FieldTypeStorage : public TypeStorage {
  /// Underlying Key type to transport the payload needed to construct a custom
  /// type in a generic way.
  using Key = std::pair<ArrayRef<int64_t>, Type>;
  /// `KeyTy` is a necessary typename hook for MLIR's custom type unique'ing.
  using KeyTy = Key;

  /// Construction in the `llvm::BumpPtrAllocator` given a key.
  static FieldTypeStorage *construct(TypeStorageAllocator &allocator,
                                     const KeyTy &key) {
    // Copy the shape into the bump pointer.
    ArrayRef<int64_t> shape = allocator.copyInto(key.first);
    // Initialize the memory using placement new.
    return new (allocator.allocate<FieldTypeStorage>())
        FieldTypeStorage(shape.size(), shape.data(), key.second);
  }

  /// Equality operator for hashing.
  bool operator==(const KeyTy &key) const {
    return key == KeyTy(getShape(), elementType);
  }

  /// Return the type of the field elements.
  Type getElementType() const { return elementType; }
  /// Return the shape of the field.
  ArrayRef<int64_t> getShape() const { return {shapeElems, shapeSize}; }

private:
  FieldTypeStorage(size_t shapeSize, const int64_t *shape, Type elementType)
      : shapeSize(shapeSize), shapeElems(shape), elementType(elementType) {}

  /// Number of shape dimensions.
  const size_t shapeSize;
  /// Shape size in each dimension.
  const int64_t *shapeElems;
  /// Type of the field elements.
  Type elementType;
};

FieldType FieldType::get(mlir::MLIRContext *context, mlir::Type elementType,
                         llvm::ArrayRef<int64_t> shape) {
  assert(shape.size() == 3 && "field shape must have 3 dimensions");
  assert((elementType.isInteger(64) || elementType.isF64()) &&
         "fields only support i64 and f64 elements");
  return Base::get(context, StenTypes::Field, shape, elementType);
}

Type FieldType::getElementType() { return getImpl()->getElementType(); }

ArrayRef<int64_t> FieldType::getShape() { return getImpl()->getShape(); }
```

Take your time to read and understand the above code, as it lays out the foundation for custom types in the Sten dialect. Note that we are storing the shape size in the `FieldTypeStorage` structure. This will make it easier to extend the `field` type once we will consider fields with less or more than three dimensions.

We now have a `field` type, but it is not registered inside our dialect. To do this, simply add the following line at the beginning of the `StenDialect` constructor:

```c++
addTypes<FieldType>();
```

Now, we need to make `mlir-opt` aware of the existence of the Sten dialect. To do this, we link `mlir-opt` to the `MLIRSten` library. In the `tools/mlir-opt/CMakeLists.txt` file, add `MLIRSten` to the list of libraries in the `LIBS` variable.

#### Testing the `field` type

Let us create a simple test file to see if our brand new type if recognized by MLIR. In `mlir/test/Dialect/Sten`, create a `types.mlir` test file and put the following content in it:

```mlir
// RUN: mlir-opt %s | FileCheck %s

func @foo(%arg0: !sten.field<?x?x?xf64>) {
	return
}
// CHECK-LABEL: func @foo(%{{.*}}: !sten.field<?x?x?xf64>) {

```

Now if we run the tests again, our two test cases fail. Let's take a look at the `types.mlir` error:

```sh
.../types.mlir:3:28: error: unknown Sten type: field<?x?x?xf64>
func @foo(%arg0: !sten.field<?x?x?xf64>) {
                           ^
```

The type should be defined now, so what is happening? We did not define a parser for this type! The error comes from this line in `StenDialect.cpp`:

```c++
parser.emitError(parser.getNameLoc(), "unknown Sten type: ")
      << parser.getFullSymbolSpec();
```

To fix this, we will need to implement a parser for the `field` type. We will also define a printer that matches the parsed pattern.

#### Parsing and printing the `field` type

Let's add the following lines to the `StenDialect::parseType` function:

```c++
// Get the type prefix
StringRef prefix;
parser.parseKeyword(&prefix);

// Parse a field type
if (prefix == "field") {
  SmallVector<int64_t, 3> shape;
  Type elementType;
  if (parser.parseLess() || parser.parseDimensionList(shape) ||
      parser.parseType(elementType) || parser.parseGreater()) {
    return Type();
  }
  // Make sure that we are only dealing with 3D fields
  if (shape.size() != 3)
    parser.emitError(parser.getNameLoc(),
                     "expected field to have three dimensions");
  return FieldType::get(getContext(), elementType, shape);
}
```

Now, let us define a custom printer for our `field` type. In the `StenDialect::printType` function, add

```c++
switch (type.getKind()) {
case StenTypes::Field:
  // Use the LLVM RTTI mechanism to dispatch to the correct function
  print(type.cast<FieldType>(), printer);
  break;
default:
  llvm_unreachable("unhandled Sten type");
}
```

and define the following function in an anonymous namespace:

```c++
void print(FieldType fieldType, DialectAsmPrinter &printer) {
  printer << "field<";
  for (auto &elem : fieldType.getShape()) {
    if (elem < 0)
      printer << "?";
    else
      printer << elem;
    printer << "x";
  }
  printer << fieldType.getElementType() << ">";
}
```

If we run the tests again, only `roundtrip.mlir` should be failing. We have successfully defined our very first Sten type!

Now it's your turn to define the `sten.view` type. It is very similar to the `field` type but I strongly encourage you to not just copy and paste the code for the `field` type, but rather to understand what steps are required to define a new type in our dialect. Make sure update the test cases and to run the tests once you are done to check if everything works as expected.

## Creating custom operations

Now that we have our custom types, we have a solid foundation on which to build the operations of our Sten dialect. Custom operations can be defined either directly in C++ or by using the declarative syntax of TableGen, the latter being the preferred way to do it. We will only focus on TableGen in this tutorial. For an example operation implemented in C++, you can have a look at the `FuncOp` operation declared in `mlir/include/mlir/IR/Function.h`.

Defining new operations in MLIR is quite straightforward and the only C++ code that we will have to provide is a parser, a printer and a verification function. But first, let's open up the `StenOps.td` file, where we will define the `sten.access` operation together.

### Defining operations in TableGen

The first thing we are going to do is to create an instance of a `Sten_Op`. Remember that `Sten_Op` was defined in `StenBase.td` as being

```tablegen
class Sten_Op<string mnemonic, list<OpTrait> traits = []> :
  Op<Sten_Dialect, mnemonic, traits> {
    let parser = [{ return ::parse$cppClass(parser, result); }];
    let printer = [{ ::print(*this, p); }];
    let verifier = [{ return ::verify(*this); }];
}
```

In TableGen, the `class` keyword defines an interface. To use this interface, we instantiate it by using the `def` keyword. Notice the `mnemonic` and `traits` parameters. These are similar to template parameters and will be used to define some properties of our operation. Let's add the following lines to `StenOps.td`:

```tablegen
def Sten_AccessOp : Sten_Op<"access"> {
  // Access operation definition goes here
}
```

Here we create an instance of `Sten_Op` with a mnemonic or name set to `access`. The name of the operation, `Sten_AccessOp`, instructs TableGen to generate a C++ class names `AccessOp` in the namespace corresponding to the `Sten` dialect, i.e. `sten`. `Sten_AccessOp` also inherits all the properties defined in `Sten_Op`.

TableGen lets us define properties of our operation by using a declarative syntax. For example, to add a brief summary as well as a description to our operation, we can write something like

```tablegen
def Sten_AccessOp : Sten_Op<"access"> {
  // Define the 'summary' property
  let summary = "view access operation";
  // Define the 'description' property
  let description = [{
    This operation takes a view as an input as well as an offset
    attribute and return the corresponding element from the view's
    underlying field. The offset is specified relatively to the
    current position.

    Example:

      %0 = sten.access %view[-1, 0, 0] : !sten.view<?x?x?xf64>
  }];
}
```

It is good practice to add this kind of short piece of documentation, including a short example, to your TableGen operation definition. Not only does it help the reader to understand what your operation is supposed to do, but it allows tools to automatically generate operation documentation for you.

The `summary` and `description` properties are defined by the base MLIR `Op` class in the `mlir/include/mlir/IR/OpBase.td` file. Other properties of interest include

- `arguments` to define the list of operation arguments types.
- `results` to define the list of return types.
- `builders` to add custom operation builders.
- `printer`, `parser` and `verifier` to add a custom printer, parser and verification function to the operation.

#### Adding the arguments

To add a list of arguments to the `sten.access` operation, we use the `arguments` property. This property has the following general syntax:

```tablegen
let arguments = (ins TypeConstraint:$paramName, AttributeConstraint:$attrName, ...)
```

Note that parameters to the operation as well as attributes are considered as arguments. But what are the `TypeConstraint` and `AttributeConstraint`? These are helpers that constrain the type of the parameter or attribute they are attached to. An example of a `TypeConstraint` is `IntOfWidths`, which constrains the type to be an integer of one of the given widths:

```tablegen
IntOfWidths<[32, 64]> // Constrain the type to be either `i32` or `i64`
```

An example of `AttributeConstraint` is `I64ArrayAttr`, which constrains the attribute to be an array of 64-bit integers.

If we take a look at the `sten.access` example,

```mlir
%0 = sten.access %view[-1, 0, 0] : !sten.view<?x?x?xf64>
```

we can see that the `access` operation has two arguments: the `%view` which is to be accessed and the offset `[-1, 0, 0]`. The former is an SSA value and the later is a compile-time known array. This distinction naturally leads to defining `%view` as a parameter to the operation and `[-1, 0, 0]` as an attribute. In fact, if you take a look at the generic form of the `access` operation, you can see that the offset is defined as an attribute named `"offset"`

```mlir
%0 = "sten.access"(%in) {offset = [-1, 0, 0]} : (!sten.view<?x?x?xf64>) -> f64
```

Fine, so let's add the `arguments` property to our `Sten_AccessOp`! But wait, what should the `TypeConstraint` for `%view` and the `AttributeConstraint` for `offset` be? We can use `I64ArrayAttr` for `offset`, but how can we constrain `%view` to be of type `sten::ViewType`? We will need to define our own type constraint for views! Let's head to the `StenBase.td` file and define it here:

```tablegen
//===----------------------------------------------------------------------===//
// Sten type definitions
//===----------------------------------------------------------------------===//

// Define a predicate that identifies a view
def Sten_IsViewType : CPred<"$_self.isa<::mlir::sten::ViewType>()">;
// Define a type constraint using the previous predicate
def Sten_View : Type<Sten_IsViewType, "a view on a field">;
```

The `Sten_IsViewType` predicate uses the TableGen symbol `$_self` to refer to the value it is attached to and calls the `isa` (read "is a") method on it to check if it is of type `ViewType`. `Sten_View` is the type constraint we will use in our argument list. A type constraint for fields might also come in handy later, so I will let you define one just after `Sten_View`.

Now that we have a proper way to constrain a value to be a `view`, let's head back to `StenOps.td` and add the following line to the definition of `Sten_AccessOp`:

```tablegen
let arguments = (ins Sten_View:$view, I64ArrayAttr:$offset);
```

#### Adding the return value

The `sten.access` operation also returns a result, which corresponds to the element type of its first argument. If we recall the syntax definition of fields and views, the only supported element types are `i64` and `f64`. We can build a type constraint for these two types using the constraint building blocks provided by MLIR. In `StenBase.td`, we can add

```tablegen
def Sten_ElementType : AnyTypeOf<[I64, F64]>;
```

Let's now add the `results` property to our `access` operation:

```tablegen
let results = (outs Sten_ElementType:$res);
```

The syntax is essentially the same as the one used for the `arguments` property.

#### Adding a custom builder

TableGen will automatically generate builder for our `access` operation based on the types of the arguments supplied in the `arguments` property. However, they have a very generic interface which is sometimes not very practical to work with[^2]. Luckily, we have a way to provide our own custom builders for our operations:

```tablegen
let builders = [OpBuilder<"Builder *, OperationState &, Value *, ArrayRef<int64_t>">];
```

To add a new builder, we use the `builders` property and define a list of `OpBuilder`s. An `OpBuilder` takes a string argument that gives the interface that the builder will have. TableGen will then generate a corresponding `build` method declaration inside the `AccessOp` class. We will only need to supply an implementation for it.

### Implementing custom building, parsing, printing and verification

If you try to compile now, you will notice that there are a few missing functions that the compiler is looking for: `parseAccessOp`, `print` and `verify`. These methods are all called because of the `parser`, `printer` and `verifier` properties in `Sten_Op`:

```tablegen
let parser = [{ return ::parse$cppClass(parser, result); }];
let printer = [{ ::print(*this, p); }];
let verifier = [{ return ::verify(*this); }];
```

 Additionally, there is also a missing implementation for our custom builder. Let's open the `StenOps.cpp` file and add the missing implementations.

#### Custom builder

Our custom builder is generated as a method in the `AccessOp` class in the `sten` namespace. Let's add the following lines to the beginning of `StenOps.cpp`:

```c++
//===----------------------------------------------------------------------===//
// sten.access
//===----------------------------------------------------------------------===//

void sten::AccessOp::build(Builder *builder, OperationState &state,
                               Value *view, ArrayRef<int64_t> offset) {
  // Make sure that the offset has the right size
  assert(offset.size() == 3 && "expected offset with 3 elements");

  // Extract the element type of the view.
  // The `cast` operation will fail and throw an error if the type of the
  // view is not `sten::ViewType`.
  Type elementType =
      view->getType().cast<sten::ViewType>().getElementType();

  // Add an SSA argument
  state.addOperands(view);
  // Add the offset attribute
  state.addAttribute("offset", builder->getI64ArrayAttr(offset));
  // Set the return type
  state.addTypes(elementType);
}
```

This builder is fine but there is a minor issue with it. The name of the `offset` attribute is hardcoded here and could potentially be accessed elsewhere. This will make maintenance more difficult as we will have to keep track of each and every location where the name `"offset"` is being used, and make sure that it corresponds to the `offset` attribute of our `access` operation.

One way to fix this problem is to make the name of the `offset` attribute be returned by a static function inside of `AccessOp`. But we don't have access to the `AccessOp` class, so how can we add such a function? Luckily, the MLIR `Op` class defined in TableGen provides an `extraClassDeclaration` property that allows us to add arbitrary pieces of code to the class' declaration. Let's add the following line to the `AccessOp` definition in `StenOps.td`:

```tablegen
let extraClassDeclaration = [{
  static StringRef getOffsetAttrName() { return "offset"; }
}];
```

We can now replace the hardcoded attribute name to a call to `getOffsetAttrName`:

```c++
// Add the offset attribute
state.addAttribute(getOffsetAttrName(), builder->getI64ArrayAttr(offset));
```

#### Custom parser

Let's add a custom parser for our operation in `StenOps.cpp`. As defined in `StenBase.td`, the parser should have the following interface:

```c++
static ParseResult parseAccessOp(OpAsmParser &parser, OperationState &state) {
  // Parsing goes here
  return success();
}
```

The role of this parser is to build an `OperationState` corresponding to the `AccessOp`. It returns `success()` if everything went fine and `failure()` if something went wrong. Once again, let's recall the syntax of the `sten.access` operation:

```mlir
%0 = sten.access %view[-1, 0, 0] : !sten.view<?x?x?xf64>
```

When the `parseAccessOp` function is called, the results and the operation name have already been parsed. So we only need to parse

```mlir
%view[-1, 0, 0] : !sten.view<?x?x?xf64>
```

Let's do it step-by-step:

```c++
static ParseResult parseAccessOp(OpAsmParser &parser, OperationState &state) {
  sten::ViewType viewType;
  Type elementType;
  ArrayAttr offset;
  // SSA values have the special type `OpAsmParser::OperandType` until they are
  // resolved
  OpAsmParser::OperandType view;

  // Step 1: Parse the `%view` operand
  if (parser.parseOperand(view))
    return failure();
  // Step 2: Parse the `offset` attribute
  if (parser.parseAttribute(offset, sten::AccessOp::getOffsetAttrName(),
                            state.attributes))
    return failure();
  // Step 2.5: Make sure it has the right number of dimensions
  if (offset.size() != 3) {
    parser.emitError(parser.getCurrentLocation(),
                     "expected offset to have three components");
    return failure();
  }

  // Step 3: Parse optional attributes as well as the view type
  // It is generally a good idea to account for an optional attribute dictionary when
  // parsing custom operations.
  if (parser.parseOptionalAttrDict(state.attributes) ||
      parser.parseColonType<sten::ViewType>(viewType))
    return failure();
  // Step 4: Make sure the `%view` operand is of the right type
  if (parser.resolveOperand(view, viewType, state.operands))
    return failure();

  // Step 5: Extract the element type from the view type
  elementType = viewType.getElementType();
  // Step 6: Add the return value
  if (parser.addTypeToList(elementType, state.types))
    return failure();

  return success();
}
```

#### Custom printer

Now that we have a parser, let's add the corresponding printer. The printer is used to print everything that should appear *after* the `=` sign assigning to results.

```c++
static void print(sten::AccessOp accessOp, OpAsmPrinter &printer) {
  // Use the TableGen'd accessors to operands
  Value *view = accessOp.view();
  Attribute offset = accessOp.offset();

  printer << sten::AccessOp::getOperationName() << ' ' << *view;
  printer.printAttribute(offset);
  // Account for an optional attribute dictionary
  printer.printOptionalAttrDict(accessOp.getAttrs(), /*elidedAttrs=*/{
                                sten::AccessOp::getOffsetAttrName()});
  printer << " : ";
  printer.printType(view->getType());
}
```

#### Custom verifier

The last missing function is the custom verifier. TableGen already generates quite a few verifications based on type constraints and operation attributes. The only thing that we did not enforce in the `Sten_AccessOp` definition is that the return type of the operation should be the same as the element type of the `%view` argument. Let's add a `verify` function to do this check for us:

```c++
static LogicalResult verify(sten::AccessOp accessOp) {
  sten::ViewType viewType =
      accessOp.view()->getType().cast<sten::ViewType>();
  Type elementType = viewType.getElementType();
  Type resultType = accessOp.getResult()->getType();

  if (resultType != elementType)
    return accessOp.emitOpError("inconsistent result type '")
           << resultType << "' and element type '" << elementType << "'";

  return success();
}
```

This is it! If you try to build the project now, there should be no error.

### Registering the operation in the dialect

To test our custom operation, we will slightly modify the `ops.mlir` test file. Let's change the run line to be

```mlir
// RUN: mlir-opt %s | mlir-opt | FileCheck %s
```

Running `mlir-opt` twice ensures that our IR *roundtrips* and that our custom printer and parser are consistent with one another. Then, we will change the `CHECK` lines corresponding to the `sten.access` operations to use our custom syntax:

```mlir
// CHECK-LABEL: func @lap(%{{.*}}: !sten.view<?x?x?xf64>) -> f64
//  CHECK-NEXT: attributes {sten.function} {
//  CHECK-NEXT: %{{.*}} = sten.access %{{.*}}[-1, 0, 0] : !sten.view<?x?x?xf64>
//  CHECK-NEXT: %{{.*}} = sten.access %{{.*}}[1, 0, 0] : !sten.view<?x?x?xf64>
//  CHECK-NEXT: %{{.*}} = sten.access %{{.*}}[0, 1, 0] : !sten.view<?x?x?xf64>
//  CHECK-NEXT: %{{.*}} = sten.access %{{.*}}[0, -1, 0] : !sten.view<?x?x?xf64>
//  CHECK-NEXT: %{{.*}} = sten.access %{{.*}}[0, 0, 0] : !sten.view<?x?x?xf64>
```

Now let's build the `check-mlir` target and see if everything works as expected.

```sh
.../ops.mlir:21:16: error: CHECK-NEXT: expected string not found in input
// CHECK-NEXT: %{{.*}} = sten.access %{{.*}}[-1, 0, 0] : !sten.view<?x?x?xf64>
               ^
<stdin>:6:2: note: scanning from here
 %0 = "sten.access"(%arg0) {offset = [-1, 0, 0]} : (!sten.view<?x?x?xf64>) -> f64
 ^
```

What happened? Why is the operation not printed using our printer? We forgot to tell our dialect that it contains a new operation! To fix this, we can add the following lines to the `StenDialect` construction

```c++
addOperations<
// Retrieve the list of TableGen operations and insert them as template parameters
#define GET_OP_LIST
#include "mlir/Dialect/Sten/StenOps.cpp.inc"
      >();
```

If we build `check-mlir` again, all the tests should be passing. Congratulations! You just created your very first custom Sten operation!

Now it's your turn to implement the remaining Sten operations. Feel free to explore the different parsing and printing facilities provided by MLIR as well as the core TableGen features that are readily available to you. Remember to update the test cases to check your implementation.

[^1]: To make sure that the interfaces remain consistent across the tutorial, we base it on commit number [165b03666355567d13ef0d7c12f535b341db5336](https://gitlab.com/open-climate-compiler/llvm-project/commit/165b03666355567d13ef0d7c12f535b341db5336) for the `llvm-project` repository and commit number [dfc345510719e4a84214c2cc512a9004439895e6](https://gitlab.com/open-climate-compiler/stencil-ir/commit/dfc345510719e4a84214c2cc512a9004439895e6) for the `stencil-ir` repository.
[^2]: We can disable the generation of default builders by setting the ``skipDefaultBuilders`` property to `1`.
