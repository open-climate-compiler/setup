#!/bin/bash

echo 'Open Climate Compiler - Development Environment Setup'
echo ''

if [[ -z $1 ]]; then
    echo 'Usage: setup.sh <occ-root-directory>'
fi

echo 'This script will setup the Open Climate Compiler (OCC) development environment on your machine.'
echo ''

export OCC_ROOT=`realpath $1`
if [ -d $OCC_ROOT ]; then
    echo "$OCC_ROOT exists. Please delete it before running this setup script"
    exit
fi
mkdir -p $OCC_ROOT
echo "Set \$OCC_ROOT to $OCC_ROOT"
while true; do
    read -p 'Check that the $OCC_ROOT directory is correct. Continue? [Y]es/[N]o: ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please enter Yes or No';;
    esac
done
echo ''

# Limiting the number of cores lowers RAM usage
while true; do
    read -p "Enter the number of build cores (Default: $(nproc)): " ncores
    if [[ -z $ncores ]]; then
        export NCORES=$(nproc)
        break
    else
        if ! [ $ncores -eq $ncores ] 2>/dev/null; then
            echo 'Please enter an integer'
        else
            export NCORES=$ncores
            break
        fi
    fi
done
echo ''

while true; do
    read -p 'Use the ninja CMake generator? [Y]es/[N]o: ' yn
    case $yn in
        [Yy]* ) export GEN=ninja; break;;
        [Nn]* ) export GEN=make; break;;
        * ) echo 'Please enter Yes or No';;
    esac
done

if [ "$GEN" = "ninja" ]; then
    export GENSTR='-G Ninja'
else
    export GENSTR='-G "Unix Makefiles"'
fi

export PREFIX=$OCC_ROOT/prefix
echo -n "Create $PREFIX... "
mkdir -p $PREFIX
echo 'done'

echo ''
echo '----------------------------------------'
echo '  LLVM and MLIR'
echo '----------------------------------------'
cd $OCC_ROOT
git clone --progress --verbose https://gitlab.com/open-climate-compiler/llvm-project.git llvm-project
git clone --progress --verbose https://gitlab.com/open-climate-compiler/stencil-ir.git llvm-project/llvm/project/mlir
cd llvm-project
mkdir build && cd build
cmake $GENSTR ../llvm -DLLVM_BUILD_EXAMPLES=OFF -DLLVM_TARGETS_TO_BUILD="host" -DCMAKE_INSTALL_PREFIX=$PREFIX
$GEN -j$NCORES all
$GEN -j$NCORES check-mlir
$GEN -j$NCORES install

echo ''
echo '----------------------------------------'
echo '  Protobuf'
echo '----------------------------------------'
cd $OCC_ROOT
git clone --progress --verbose https://github.com/protocolbuffers/protobuf.git protobuf
cd protobuf
mkdir build && cd build
cmake $GENSTR ../cmake -Dprotobuf_BUILD_TESTS=OFF -Dprotobuf_BUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=$PREFIX
$GEN -j$NCORES install

echo ''
echo '----------------------------------------'
echo '  Dawn'
echo '----------------------------------------'
cd $OCC_ROOT
git clone --progress --verbose https://gitlab.com/open-climate-compiler/dawn.git dawn
cd dawn
mkdir build && cd build
cmake $GENSTR .. -DProtobuf_DIR=$PREFIX/lib/cmake/protobuf -DCMAKE_INSTALL_PREFIX=$PREFIX
$GEN -j$NCORES install
